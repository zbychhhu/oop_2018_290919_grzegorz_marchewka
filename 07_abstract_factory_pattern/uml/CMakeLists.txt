
find_program(PLATUML plantuml)

if (NOT PLATUML)
    message(FATAL_ERROR "Missing PlatUML binary!")
endif()

set(DIAGRAM_FIRST_PUML ${CMAKE_CURRENT_SOURCE_DIR}/first.puml)
set(DIAGRAM_FIRST_SVG ${CMAKE_CURRENT_BINARY_DIR}/first.svg)

add_custom_command(
    OUTPUT ${DIAGRAM_FIRST_SVG}
    COMMAND ${PLATUML} -o ${CMAKE_CURRENT_BINARY_DIR} -tsvg ${DIAGRAM_FIRST_PUML}
    MAIN_DEPENDENCY ${DIAGRAM_FIRST_PUML}
    COMMENT "Rendering ${DIAGRAM_FIRST_SVG} from ${DIAGRAM_FIRST_PUML}")

set(DIAGRAM_SECOND_PUML ${CMAKE_CURRENT_SOURCE_DIR}/second.puml)
set(DIAGRAM_SECOND_SVG ${CMAKE_CURRENT_BINARY_DIR}/second.svg)

add_custom_command(
    OUTPUT ${DIAGRAM_SECOND_SVG}
    COMMAND ${PLATUML} -o ${CMAKE_CURRENT_BINARY_DIR} -tsvg ${DIAGRAM_SECOND_PUML}
    MAIN_DEPENDENCY ${DIAGRAM_SECOND_PUML}
    COMMENT "Rendering ${DIAGRAM_SECOND_SVG} from ${DIAGRAM_SECOND_PUML}")

add_custom_target(07_abstract_factory_pattern_uml DEPENDS ${DIAGRAM_FIRST_SVG} ${DIAGRAM_SECOND_SVG})
