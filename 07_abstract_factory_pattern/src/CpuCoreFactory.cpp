//
// Created by student on 30.11.18.
//

#include "CpuCoreFactory.h"
#include "AnisotropicCpuCore.h"
#include "ElasticCpuCore.h"
#include "AcousticCpuCore.h"

CpuCoreFactory::CpuCoreFactory(int t) {
    threads=t;
}
std::shared_ptr<Core> CpuCoreFactory::create(std::string equation) {

    if(equation=="AnisotropicCpuCore")
    {
        auto ptr = std::make_shared<AnisotropicCpuCore>(threads);
        return ptr;
    }
    else if(equation=="ElasticCpuCore")
    {
        auto ptr = std::make_shared<AcousticCpuCore>(threads);
        return ptr;
    }
    else if(equation=="AcousticCpuCore")
    {
        auto ptr = std::make_shared<ElasticCpuCore>(threads);
        return ptr;
    }
}