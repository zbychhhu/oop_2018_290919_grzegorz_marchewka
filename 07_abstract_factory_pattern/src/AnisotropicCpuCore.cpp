//
// Created by student on 30.11.18.
//

#include "AnisotropicCpuCore.h"

AnisotropicCpuCore::AnisotropicCpuCore(int t) {
    threads=t;
}
void AnisotropicCpuCore::execute() {
    std::cout << " I am AnisotropicCpuCore with threads: " << threads << std::endl;
}