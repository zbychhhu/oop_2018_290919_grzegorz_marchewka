#include <iostream>
#include <memory>
#include "Dummy.h"
#include "Demo.h"
#include "CoreFactory.h"
#include "Core.h"
#include "CpuCoreFactory.h"
#include "CudaCoreFactory.h"

int main() {


    auto cudaCoreFactory=std::make_shared<CudaCoreFactory>(13);
    auto cpuCoreFactory = std::make_shared<CpuCoreFactory>(21);
    Demo demoCuda=Demo(cudaCoreFactory);
    Demo demoCpu=Demo(cpuCoreFactory);
    demoCuda.run("ElasticCudaCore");
    demoCpu.run("AcousticCpuCore");
    demoCpu.run("AnisotropicCpuCore");


    return 0;
}
