//
// Created by student on 30.11.18.
//

#include "CudaCoreFactory.h"
#include "AnisotropicCudaCore.h"
#include "AcousticCudaCore.h"

#include "ElasticCudaCore.h"


CudaCoreFactory::CudaCoreFactory(int g) {
    gpuid=g;
}
std::shared_ptr<Core> CudaCoreFactory::create(std::string equation) {
    if(equation=="AnisotropicCudaCore")
    {
        auto ptr = std::make_shared<AnisotropicCudaCore>(gpuid);
        return ptr;
    }
    else if(equation=="ElasticCudaCore")
    {
        auto ptr = std::make_shared<ElasticCudaCore>(gpuid);
        return ptr;
    }
    else if(equation=="AcousticCudaCore")
    {
        auto ptr = std::make_shared<AcousticCudaCore>(gpuid);
        return ptr;
    }

}