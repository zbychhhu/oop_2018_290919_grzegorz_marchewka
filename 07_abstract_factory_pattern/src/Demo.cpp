//
// Created by student on 30.11.18.
//

#include "Demo.h"

Demo::Demo(std::shared_ptr<CoreFactory> factory) {
    this->factory=factory;
}
void Demo::run(std::string equation) {
   factory.get()->create(equation)->execute();

}