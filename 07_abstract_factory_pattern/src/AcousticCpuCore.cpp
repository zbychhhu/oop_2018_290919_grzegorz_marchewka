//
// Created by student on 30.11.18.
//

#include "AcousticCpuCore.h"
AcousticCpuCore::AcousticCpuCore(int t) {
    threads = t;
}
void AcousticCpuCore::execute() {
    std::cout << " I am AcousticCpuCore with threads: " << threads << std::endl;
}