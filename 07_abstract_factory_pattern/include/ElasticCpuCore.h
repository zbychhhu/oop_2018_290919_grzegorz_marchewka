//
// Created by student on 30.11.18.
//

#ifndef INC_07_ABSTRACT_FACTORY_PATTERN_ELASTICCPUCORE_H
#define INC_07_ABSTRACT_FACTORY_PATTERN_ELASTICCPUCORE_H
#include "Core.h"

class ElasticCpuCore:public Core {
private:
    int threads;
public:
    ElasticCpuCore(int);
    void execute();
};


#endif //INC_07_ABSTRACT_FACTORY_PATTERN_ELASTICCPUCORE_H
