//
// Created by student on 30.11.18.
//

#ifndef INC_07_ABSTRACT_FACTORY_PATTERN_CORE_H
#define INC_07_ABSTRACT_FACTORY_PATTERN_CORE_H
#include <iostream>
#include <string>
#include <memory>

class Core{
public:
    virtual void execute()=0;
};
#endif //INC_07_ABSTRACT_FACTORY_PATTERN_CORE_H
