//
// Created by student on 30.11.18.
//

#ifndef INC_07_ABSTRACT_FACTORY_PATTERN_ELASTICCUDACORE_H
#define INC_07_ABSTRACT_FACTORY_PATTERN_ELASTICCUDACORE_H
#include "Core.h"

class ElasticCudaCore:public Core {
private:
    int gpuid;
public:
    ElasticCudaCore(int);
    void execute();
};


#endif //INC_07_ABSTRACT_FACTORY_PATTERN_ELASTICCUDACORE_H
