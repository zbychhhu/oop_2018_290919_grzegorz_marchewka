//
// Created by student on 30.11.18.
//

#ifndef INC_07_ABSTRACT_FACTORY_PATTERN_CPUCOREFACTORY_H
#define INC_07_ABSTRACT_FACTORY_PATTERN_CPUCOREFACTORY_H


#include "CoreFactory.h"

class CpuCoreFactory :public CoreFactory{
private:
    int threads;
public:
    CpuCoreFactory(int t);
    std::shared_ptr<Core> create(std::string equation);
};


#endif //INC_07_ABSTRACT_FACTORY_PATTERN_CPUCOREFACTORY_H
