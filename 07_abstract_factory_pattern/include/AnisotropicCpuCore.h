//
// Created by student on 30.11.18.
//

#ifndef INC_07_ABSTRACT_FACTORY_PATTERN_ANISOTROPICCPUCORE_H
#define INC_07_ABSTRACT_FACTORY_PATTERN_ANISOTROPICCPUCORE_H


#include "Core.h"

class AnisotropicCpuCore:public Core {
private:
    int threads;
public:
    AnisotropicCpuCore(int);
    void execute();
};



#endif //INC_07_ABSTRACT_FACTORY_PATTERN_ANISOTROPICCPUCORE_H
