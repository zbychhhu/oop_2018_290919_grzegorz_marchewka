//
// Created by student on 30.11.18.
//

#ifndef INC_07_ABSTRACT_FACTORY_PATTERN_DEMO_H
#define INC_07_ABSTRACT_FACTORY_PATTERN_DEMO_H

#include "CoreFactory.h"

class Demo {
private:
    std::shared_ptr<CoreFactory> factory;
public:
    Demo(std::shared_ptr<CoreFactory> factory);
    void run(std::string equation);
};


#endif //INC_07_ABSTRACT_FACTORY_PATTERN_DEMO_H
