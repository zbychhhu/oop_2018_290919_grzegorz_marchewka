//
// Created by student on 30.11.18.
//

#ifndef INC_07_ABSTRACT_FACTORY_PATTERN_ANISOTROPICCUDACORE_H
#define INC_07_ABSTRACT_FACTORY_PATTERN_ANISOTROPICCUDACORE_H

#include "Core.h"

class AnisotropicCudaCore:public Core {
private:
    int gpuid;
public:
    AnisotropicCudaCore(int);
    void execute();
};


#endif //INC_07_ABSTRACT_FACTORY_PATTERN_ANISOTROPICCUDACORE_H
