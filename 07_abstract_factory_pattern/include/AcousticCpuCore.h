//
// Created by student on 30.11.18.
//

#ifndef INC_07_ABSTRACT_FACTORY_PATTERN_ACOUSTICCPUCORE_H
#define INC_07_ABSTRACT_FACTORY_PATTERN_ACOUSTICCPUCORE_H

#include "Core.h"

class AcousticCpuCore:public Core {
private:
    int threads;
public:
    AcousticCpuCore(int);
    void execute();
};


#endif //INC_07_ABSTRACT_FACTORY_PATTERN_ACOUSTICCPUCORE_H
