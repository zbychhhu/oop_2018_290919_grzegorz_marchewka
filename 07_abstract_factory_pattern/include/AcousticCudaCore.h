//
// Created by student on 30.11.18.
//

#ifndef INC_07_ABSTRACT_FACTORY_PATTERN_ACOUSTICCUDACORE_H
#define INC_07_ABSTRACT_FACTORY_PATTERN_ACOUSTICCUDACORE_H

#include "Core.h"

class AcousticCudaCore:public Core {
private:
    int gpuid;
public:
    AcousticCudaCore(int);
    void execute();
};


#endif //INC_07_ABSTRACT_FACTORY_PATTERN_ACOUSTICCUDACORE_H
