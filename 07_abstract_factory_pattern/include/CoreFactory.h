//
// Created by student on 30.11.18.
//

#ifndef INC_07_ABSTRACT_FACTORY_PATTERN_COREFACTORY_H
#define INC_07_ABSTRACT_FACTORY_PATTERN_COREFACTORY_H

#include <memory>
#include "Core.h"


class CoreFactory{
public:
    virtual std::shared_ptr<Core> create(std::string equation)=0;
};
#endif //INC_07_ABSTRACT_FACTORY_PATTERN_COREFACTORY_H
