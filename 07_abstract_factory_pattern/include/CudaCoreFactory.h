//
// Created by student on 30.11.18.
//

#ifndef INC_07_ABSTRACT_FACTORY_PATTERN_CUDACOREFACTORY_H
#define INC_07_ABSTRACT_FACTORY_PATTERN_CUDACOREFACTORY_H

#include "CoreFactory.h"
class CudaCoreFactory :public CoreFactory{
private:
    int gpuid;
public:
    CudaCoreFactory(int g);
    std::shared_ptr<Core> create(std::string equation);
};


#endif //INC_07_ABSTRACT_FACTORY_PATTERN_CUDACOREFACTORY_H
