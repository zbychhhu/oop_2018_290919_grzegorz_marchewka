#include "TestIncludes.h"
#include "TextWrap.h"

TEST(TextWrap, Create)
{
    TextWrap textWrap{};
}


TEST(TextWrap, Columns_Default)
{
    TextWrap textWrap{};
    EXPECT_EQ(10,textWrap.columns());
}


TEST(TextWrap, Columns_CustomNumber)
{
    TextWrap textWrap{3};
    EXPECT_EQ(3,textWrap.columns());
}

TEST(TextWrap, Wrap_singlecolumn_Empty)
{
    TextWrap textWrap{1};
    EXPECT_EQ("",textWrap.wrap(""));
}
TEST(TextWrap, Wrap_singlecolumn_SingleChar)
{
    TextWrap textWrap{1};
    EXPECT_EQ("a",textWrap.wrap("a"));
}
TEST(TextWrap, Wrap_singlecolumn_TwoChars)
{
    TextWrap textWrap{1};
    EXPECT_EQ("a\nb",textWrap.wrap("ab"));
}

TEST(TextWrap, Wrap_singlecolumn_ThreeChars)
{
    TextWrap textWrap{1};
    EXPECT_EQ("a\nb\nc",textWrap.wrap("abc"));
}

//TEST(TextWrap, Wrap_singlecolumn_FourChars)
//{
//    TextWrap textWrap{2};
//    EXPECT_EQ("ab\ncd",textWrap.wrap("abcd"));
//}
//TEST(TextWrap, Wrap_singlecolumn_FourChars_WithSpace)
//{
//    TextWrap textWrap{2};
//    EXPECT_EQ("ab\ncd",textWrap.wrap("ab cd"));
//}
//TEST(TextWrap, Wrap_singlecolumn_FiveChars_WithSpace)
//{
//    TextWrap textWrap{3};
//    EXPECT_EQ("a\nabc\nd",textWrap.wrap("a abc d"));
//}
