#pragma once
#include <string>

class TextWrap
{
public:
    explicit TextWrap(int columns_ = 10)
    : columns_(columns_)
    {}
    int columns() const {
        return columns_;
    }
    std::string wrap(const std::string& s) const{

        if(s.length() > columns_)
            return s.substr(0,columns_ ) + "\n" +s.substr(columns_);

        return s;
    }

private:
    int columns_;
};