#pragma once

#include <random>

struct Large {

    constexpr static unsigned SIZE = 128u*1024u;

    double data[SIZE];

    void clear() {
        std::fill_n(data, SIZE, 0);
    }

    void randomize() {
        static std::random_device rd{};
        static std::mt19937 gen{rd()};
        static std::uniform_real_distribution<> dis{};

        for (double &i : data) {
            i = dis(gen);
        }
    }

    bool operator<(const Large &rhs) const {

        // TODO: Implement me!
        double first=0;
        double second=0;
        for(int i=0;i<rhs.SIZE;i++)
        {
            first+=data[i];
            second+=rhs.data[i];
        }

        return first<second;


    }

    bool operator==(const Large &rhs) const {

        // TODO: Implement me!
        return std::equal(std::begin(data), std::end(data), std::begin(rhs.data));
    }
};

namespace std {
    template<>
    struct hash<Large> {
        std::size_t operator()(const Large &d) const {

            // TODO: Implement me!
            hash<double> hash;
            size_t result = 0;
            for (const auto& element: d.data) {
                result = result * 19 + hash(element);

            }
            return result;
        }
    };
}
