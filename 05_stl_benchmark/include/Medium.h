#pragma once

#include <random>

struct Medium {

    constexpr static unsigned SIZE = 256u;
    int data[SIZE];

    void clear() {
        std::fill_n(data, SIZE, 0);
    }

    void randomize() {
        static std::random_device rd{};
        static std::mt19937 gen{rd()};
        static std::uniform_int_distribution<int> dis{};

        for (int &i : data)
            i = dis(gen);
    }

    bool operator<(const Medium &rhs) const {

        // TODO: Implement me!
        int first=0;
        int second=0;
        for(int i=0;i<rhs.SIZE;i++)
        {
            first+=data[i];
            second+=rhs.data[i];
        }

        return (first<second);

    }

    bool operator==(const Medium &rhs) const {

        // TODO: Implement me!
        return std::equal(std::begin(data), std::end(data), std::begin(rhs.data));

    }
};

namespace std {
    template<>
    struct hash<Medium> {
        std::size_t operator()(const Medium &d) const {

            // TODO: Implement me!
            hash<int> hash;
            size_t result = 0;
            for (const auto& element: d.data) {
                result = result * 33 + hash(element);

            }
            return result;
        }
    };
}