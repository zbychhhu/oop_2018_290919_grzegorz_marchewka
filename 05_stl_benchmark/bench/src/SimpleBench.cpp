#include "BenchIncludes.h"

static void demo(State& state)
{
    auto N=state.range(0);
    int a=10;
    int b=20;

    for (auto _ : state)
    {
        int count{};

        for (int i=0;i<N;i++)
            count++;

        DoNotOptimize(count);
    }
    state.SetComplexityN(N);
}
BENCHMARK(demo)->RangeMultiplier(2)->Range(1,1024)->Complexity();