#include "Large.h"
#include "BenchIncludes.h"
#include <list>
#include <set>
#include <unordered_set>

// TODO: Add benchmarks for operator<. operator==, and hash

void lessOperatorLarge(State &state)
{

    Large large{};
    Large large2{};
    large.randomize();
    large2.randomize();
    bool condition;
    for(auto _ : state)
    {
        condition=large<large2;
        DoNotOptimize(condition);
    }
}
BENCHMARK(lessOperatorLarge)->RangeMultiplier(2)->Range(1,1024);

void equalityLarge(State &state)
{

    Large large{};
    Large large2{};
    large.randomize();
    large2.randomize();
    bool condition;
    for(auto _ : state)
    {
        condition=large==large2;
        DoNotOptimize(condition);
    }

}
BENCHMARK(equalityLarge)->RangeMultiplier(2)->Range(1,1024);

void hashLarge(State &state)
{

    Large large{};
    std::hash<Large> hash;
    large.randomize();
    for(auto _ : state)
    {
        hash(large);
        DoNotOptimize(large);

    }
}
BENCHMARK(hashLarge)->RangeMultiplier(2)->Range(1,1024);

void sizeListLarge(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Large> list(size);
    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        list.push_back(large[i]);
    }

    for (auto _ : state) {
        DoNotOptimize(list.size());

    }

    state.SetComplexityN(N);
}

BENCHMARK(sizeListLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void frontListLarge(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Large> list(size);
    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        list.push_back(large[i]);
    }

    for (auto _ : state) {
        auto t=list.front();
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(frontListLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void backListLarge(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Large> list(size);
    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        list.push_back(large[i]);
    }

    for (auto _ : state) {
        auto t=list.back();
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(backListLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void emptyListLarge(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Large> list(size);
    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        list.push_back(large[i]);
    }

    for (auto _ : state) {
        auto t=list.empty();
        DoNotOptimize(t);

    }

    state.SetComplexityN(N);
}

BENCHMARK(emptyListLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void max_sizeListLarge(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Large> list(size);
    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        list.push_back(large[i]);
    }

    for (auto _ : state) {
        auto t=list.max_size();
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(max_sizeListLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void eraseListLarge(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Large> list(size);
    auto it = list.begin();
    for (auto _ : state) {
        list.push_back(Large{2});
        auto t=list.erase(it);
        DoNotOptimize(t);


    }

    state.SetComplexityN(N);
}

BENCHMARK(eraseListLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void insertListLarge(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Large> list(size);
    Large large[N];
    large[0].data[0]='1';
    auto el =large[0];
    auto iter = list.begin();

    for (auto _ : state) {
        DoNotOptimize(list);

        auto t=list.insert(iter,el);
    }

    state.SetComplexityN(N);
}

BENCHMARK(insertListLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void clearListLarge(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Large> list(size);
    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        list.push_back(large[i]);
    }

    for (auto _ : state) {
        DoNotOptimize(list);
        list.clear();
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(clearListLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void push_frontAndpop_frontListLarge(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;

    Large large{};
    *large.data = '2';
    std::list<Large> list(size);

    for (auto _ : state) {
        DoNotOptimize(list);
        list.push_front(large);
        list.pop_front();
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(push_frontAndpop_frontListLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void push_backAndpop_backListLarge(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;

    Large large{};
    *large.data = '2';
    std::list<Large> list(size);

    for (auto _ : state) {
        DoNotOptimize(list);
        list.push_back(large);
        list.pop_back();
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(push_backAndpop_backListLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void resizeListLarge(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Large> list(size);
    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        list.push_back(large[i]);
    }

    for (auto _ : state) {
        DoNotOptimize(list);
        list.resize(3);
        ClobberMemory();
    }


    state.SetComplexityN(N);
}

BENCHMARK(resizeListLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void swapListLarge(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Large> list(size);
    std::list<Large> list2(size);
    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        list.push_back(large[i]);
        list2.push_front(large[i]);
    }
    for (auto _ : state) {

        list.swap(list2);
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(swapListLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();


void mergeListLarge(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Large> list(size);
    std::list<Large> list2(size);
    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        list.push_back(large[i]);
        list2.push_front(large[i]);
    }

    for (auto _ : state) {
        DoNotOptimize(list);
        DoNotOptimize(list2);
        list.merge(list2);
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(mergeListLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void spliceListLarge(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Large> list(size);
    std::list<Large> list2(size);
    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        list.push_back(large[i]);
        list2.push_front(large[i]);
    }
    auto it = list.begin();
    for (auto _ : state) {

        DoNotOptimize(list);
        DoNotOptimize(list2);
        list.splice(it, list2);
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(spliceListLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void removeListLarge(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Large> list(size);
    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        list.push_back(large[i]);
    }
    auto el = large[0];


    for (auto _ : state) {

        DoNotOptimize(list);
        list.remove(el);
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(removeListLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void remove_ifListLarge(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Large> list(size);
    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        list.push_back(large[i]);
    }


    for (auto _ : state) {
        DoNotOptimize(list);
        list.remove_if([](Large n) { return n < Large{'1'}; });
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(remove_ifListLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void reverseListLarge(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Large> list(size);
    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        list.push_back(large[i]);
    }


    for (auto _ : state) {
        DoNotOptimize(list);
        list.reverse();
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(reverseListLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void sortListLarge(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Large> list(size);
    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        list.push_back(large[i]);
    }


    for (auto _ : state) {
        DoNotOptimize(list);
        list.sort();
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(sortListLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void uniqueListLarge(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Large> list(size);
    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        list.push_back(large[i]);
    }


    for (auto _ : state) {

        DoNotOptimize(list);
        list.unique();
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(uniqueListLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void sizeMultiSetLarge(State &state) {


    auto N = state.range(0);

    std::multiset<Large> multiset;
    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        multiset.insert(large[i]);
    }


    for (auto _ : state) {
        auto t=multiset.size();
        DoNotOptimize(t);

    }

    state.SetComplexityN(N);
}

BENCHMARK(sizeMultiSetLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void max_sizeMultiSetLarge(State &state) {


    auto N = state.range(0);


    std::multiset<Large> multiset;
    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        multiset.insert(large[i]);
    }
    for (auto _ : state) {
        auto t = multiset.max_size();
        DoNotOptimize(t);

    }

    state.SetComplexityN(N);
}

BENCHMARK(max_sizeMultiSetLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void emptyMultiSetLarge(State &state) {


    auto N = state.range(0);



    std::multiset<Large> multiset;
    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        multiset.insert(large[i]);
    }
    for (auto _ : state) {

        auto t=multiset.empty();
        DoNotOptimize(t);

    }

    state.SetComplexityN(N);
}

BENCHMARK(emptyMultiSetLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void clearMultiSetLarge(State &state) {


    auto N = state.range(0);



    std::multiset<Large> multiset;
    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        multiset.insert(large[i]);
    }
    for (auto _ : state) {

        DoNotOptimize(multiset);

        multiset.clear();
        ClobberMemory();

    }

    state.SetComplexityN(N);
}

BENCHMARK(clearMultiSetLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void insertMultiSetLarge(State &state) {


    auto N = state.range(0);


    std::multiset<Large> multiset;
    Large large;
    large.data[0]='1';

    for (auto _ : state) {

        auto t=multiset.insert(large);
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(insertMultiSetLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void eraseMultiSetLarge(State &state) {


    auto N = state.range(0);


    std::multiset<Large> multiset;
    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        multiset.insert(large[i]);
    }
    auto el = large[0];
    for (auto _ : state) {
        auto t=multiset.erase(el);
        DoNotOptimize(t);

    }

    state.SetComplexityN(N);
}

BENCHMARK(eraseMultiSetLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void swapMultiSetLarge(State &state) {


    auto N = state.range(0);


    std::multiset<Large> multiset;

    std::multiset<Large> multiset2;
    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        multiset.insert(large[i]);
        multiset2.insert(large[i]);
    }

    for (auto _ : state) {
        DoNotOptimize(multiset);
        multiset.swap(multiset2);
        ClobberMemory();

    }

    state.SetComplexityN(N);
}

BENCHMARK(swapMultiSetLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void countMultiSetLarge(State &state) {


    auto N = state.range(0);


    std::multiset<Large> multiset;
    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        multiset.insert(large[i]);
    }
    auto el =large[0];

    for (auto _ : state) {

        auto t=  multiset.count(el);
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(countMultiSetLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void findMultiSetLarge(State &state) {


    auto N = state.range(0);


    std::multiset<Large> multiset;
    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        multiset.insert(large[i]);
    }


    for (auto _ : state) {
        auto t=multiset.find(Large{2});
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(findMultiSetLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void equal_rangeMultiSetLarge(State &state) {


    auto N = state.range(0);


    std::multiset<Large> multiset;

    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        multiset.insert(large[i]);
    }
    auto el =large[0];

    for (auto _ : state) {
        auto t=multiset.equal_range(el);
        DoNotOptimize(t);

    }

    state.SetComplexityN(N);
}

BENCHMARK(equal_rangeMultiSetLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void upper_boundMultiSetLarge(State &state) {


    auto N = state.range(0);

    std::multiset<Large> multiset;
    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        multiset.insert(large[i]);
    }
    auto el =large[0];


    for (auto _ : state) {
        auto t=multiset.upper_bound(el);
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(upper_boundMultiSetLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void sizeUnordered_MultiSetLarge(State &state) {


    auto N = state.range(0);

    std::unordered_multiset<Large> unordered_multiset;
    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        unordered_multiset.insert(large[i]);
    }



    for (auto _ : state) {
        auto t = unordered_multiset.size();
        DoNotOptimize(t);

    }

    state.SetComplexityN(N);
}

BENCHMARK(sizeUnordered_MultiSetLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void max_sizeUnordered_MultiSetLarge(State &state) {


    auto N = state.range(0);


    std::unordered_multiset<Large> unordered_multiset;
    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        unordered_multiset.insert(large[i]);
    }

    for (auto _ : state) {
        auto t = unordered_multiset.max_size();
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(max_sizeUnordered_MultiSetLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void emptyUnordered_MultiSetLarge(State &state) {


    auto N = state.range(0);


    std::unordered_multiset<Large> unordered_multiset;
    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        unordered_multiset.insert(large[i]);
    }
    for (auto _ : state) {
        auto t = unordered_multiset.empty();
        DoNotOptimize(t);

    }

    state.SetComplexityN(N);
}

BENCHMARK(emptyUnordered_MultiSetLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void clearUnordered_MultiSetLarge(State &state) {


    auto N = state.range(0);


    std::unordered_multiset<Large> unordered_multiset;
    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        unordered_multiset.insert(large[i]);
    }
    for (auto _ : state) {
        DoNotOptimize(unordered_multiset);

        unordered_multiset.clear();
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(clearUnordered_MultiSetLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void insertUnordered_MultiSetLarge(State &state) {


    auto N = state.range(0);


    std::unordered_multiset<Large> unordered_multiset;
    Large large;
    large.data[0]='1';
    for (auto _ : state) {
        DoNotOptimize(unordered_multiset);

        auto t=unordered_multiset.insert(large);
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(insertUnordered_MultiSetLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void eraseUnordered_MultiSetLarge(State &state) {


    auto N = state.range(0);



    std::unordered_multiset<Large> unordered_multiset;
    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        unordered_multiset.insert(large[i]);
    }
    auto el = large[0];
    for (auto _ : state) {
        auto t=unordered_multiset.erase(el);
        DoNotOptimize(t);

    }

    state.SetComplexityN(N);
}

BENCHMARK(eraseUnordered_MultiSetLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void swapUnordered_MultiSetLarge(State &state) {


    auto N = state.range(0);


    std::unordered_multiset<Large> unordered_multiset;
    std::unordered_multiset<Large> unordered_multiset2;
    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        unordered_multiset.insert(large[i]);
        unordered_multiset2.insert(large[i]);
    }

    for (auto _ : state) {
        DoNotOptimize(unordered_multiset);

        unordered_multiset.swap(unordered_multiset2);
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(swapUnordered_MultiSetLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void countUnordered_MultiSetLarge(State &state) {


    auto N = state.range(0);


    std::unordered_multiset<Large> unordered_multiset;
    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        unordered_multiset.insert(large[i]);
    }
    auto el=large[0];

    for (auto _ : state) {

        auto t=unordered_multiset.count(el);
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(countUnordered_MultiSetLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void findUnordered_MultiSetLarge(State &state) {


    auto N = state.range(0);


    std::unordered_multiset<Large> unordered_multiset = {{Large{2}}};
    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        unordered_multiset.insert(large[i]);
    }
    auto el=large[0];


    for (auto _ : state) {


        auto t=unordered_multiset.find(el);
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(findUnordered_MultiSetLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void equal_rangeUnordered_MultiSetLarge(State &state) {


    auto N = state.range(0);


    std::unordered_multiset<Large> unordered_multiset;
    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        unordered_multiset.insert(large[i]);
    }
    auto el=large[0];


    for (auto _ : state) {

        auto t=unordered_multiset.equal_range(el);
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(equal_rangeUnordered_MultiSetLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void reserveUnordered_MultiSetLarge(State &state) {


    auto N = state.range(0);


    std::unordered_multiset<Large> unordered_multiset;
    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        unordered_multiset.insert(large[i]);
    }

    for (auto _ : state) {
        DoNotOptimize(unordered_multiset);

        unordered_multiset.reserve(5);
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(reserveUnordered_MultiSetLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();

void rehashUnordered_MultiSetLarge(State &state) {


    auto N = state.range(0);

    std::unordered_multiset<Large> unordered_multiset;
    Large large[N];
    for(unsigned int i =0;i<N;i++)
    {
        large[i].randomize();
        unordered_multiset.insert(large[i]);
    }
    for (auto _ : state) {
        DoNotOptimize(unordered_multiset);
        unordered_multiset.rehash(7);
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(rehashUnordered_MultiSetLarge)->RangeMultiplier(2)->Range(1, 4)->Complexity();
