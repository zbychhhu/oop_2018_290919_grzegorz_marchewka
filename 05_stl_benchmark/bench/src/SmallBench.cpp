#include "Small.h"
#include "BenchIncludes.h"
#include <list>
#include <set>
#include <unordered_set>

// TODO: Add benchmarks for operator<. operator==, and hash
void lessOperatorSmall(State &state) {

    Small small{};
    Small small2{};
    small.randomize();
    small2.randomize();
    bool condition;
    for (auto _ : state) {
        condition = small < small2;
        DoNotOptimize(condition);
    }
}

BENCHMARK(lessOperatorSmall)->RangeMultiplier(2)->Range(1, 512);

void equalitySmall(State &state) {

    Small small{};
    Small small2{};
    small.randomize();
    small2.randomize();
    bool condition;
    for (auto _ : state) {
        condition = small == small2;
        DoNotOptimize(condition);
    }
}

BENCHMARK(equalitySmall)->RangeMultiplier(2)->Range(1, 512);

void hashSmall(State &state) {

    Small small{};
    std::hash<Small> hash;
    small.randomize();
    for (auto _ : state) {
        hash(small);
        DoNotOptimize(small);

    }

}

BENCHMARK(hashSmall)->RangeMultiplier(2)->Range(1, 512);

void sizeListSmall(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Small> list(size);
    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        list.push_back(small[i]);
    }

    for (auto _ : state) {
        DoNotOptimize(list.size());

    }

    state.SetComplexityN(N);
}

BENCHMARK(sizeListSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void frontListSmall(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Small> list(size);
    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        list.push_back(small[i]);
    }

    for (auto _ : state) {
        auto t=list.front();
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(frontListSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void backListSmall(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Small> list(size);
    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        list.push_back(small[i]);
    }

    for (auto _ : state) {
        auto t=list.back();
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(backListSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void emptyListSmall(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Small> list(size);
    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        list.push_back(small[i]);
    }

    for (auto _ : state) {
        auto t=list.empty();
        DoNotOptimize(t);

    }

    state.SetComplexityN(N);
}

BENCHMARK(emptyListSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void max_sizeListSmall(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Small> list(size);
    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        list.push_back(small[i]);
    }

    for (auto _ : state) {
        auto t=list.max_size();
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(max_sizeListSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void eraseListSmall(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Small> list(size);
    auto it = list.begin();
    for (auto _ : state) {
        list.push_back(Small{2});
        auto t=list.erase(it);
        DoNotOptimize(t);


    }

    state.SetComplexityN(N);
}

BENCHMARK(eraseListSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void insertListSmall(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Small> list(size);
    Small small[N];
    small[0].data[0]='1';
    auto el =small[0];
    auto iter = list.begin();

    for (auto _ : state) {
        DoNotOptimize(list);

        auto t=list.insert(iter,el);
    }

    state.SetComplexityN(N);
}

BENCHMARK(insertListSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void clearListSmall(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Small> list(size);
    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        list.push_back(small[i]);
    }

    for (auto _ : state) {
        DoNotOptimize(list);
        list.clear();
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(clearListSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void push_frontAndpop_frontListSmall(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;

    Small small{};
    *small.data = '2';
    std::list<Small> list(size);

    for (auto _ : state) {
        DoNotOptimize(list);
        list.push_front(small);
        list.pop_front();
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(push_frontAndpop_frontListSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void push_backAndpop_backListSmall(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;

    Small small{};
    *small.data = '2';
    std::list<Small> list(size);

    for (auto _ : state) {
        DoNotOptimize(list);
        list.push_back(small);
        list.pop_back();
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(push_backAndpop_backListSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void resizeListSmall(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Small> list(size);
    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        list.push_back(small[i]);
    }

    for (auto _ : state) {
        DoNotOptimize(list);
        list.resize(3);
        ClobberMemory();
    }


    state.SetComplexityN(N);
}

BENCHMARK(resizeListSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void swapListSmall(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Small> list(size);
    std::list<Small> list2(size);
    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        list.push_back(small[i]);
        list2.push_front(small[i]);
    }
    for (auto _ : state) {

        list.swap(list2);
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(swapListSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();


void mergeListSmall(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Small> list(size);
    std::list<Small> list2(size);
    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        list.push_back(small[i]);
        list2.push_front(small[i]);
    }

    for (auto _ : state) {
        DoNotOptimize(list);
        DoNotOptimize(list2);
        list.merge(list2);
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(mergeListSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void spliceListSmall(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Small> list(size);
    std::list<Small> list2(size);
    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        list.push_back(small[i]);
        list2.push_front(small[i]);
    }
    auto it = list.begin();
    for (auto _ : state) {

        DoNotOptimize(list);
        DoNotOptimize(list2);
        list.splice(it, list2);
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(spliceListSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void removeListSmall(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Small> list(size);
    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        list.push_back(small[i]);
    }
    auto el = small[0];


    for (auto _ : state) {

        DoNotOptimize(list);
        list.remove(el);
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(removeListSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void remove_ifListSmall(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Small> list(size);
    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        list.push_back(small[i]);
    }


    for (auto _ : state) {
        DoNotOptimize(list);
        list.remove_if([](Small n) { return n < Small{'1'}; });
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(remove_ifListSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void reverseListSmall(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Small> list(size);
    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        list.push_back(small[i]);
    }


    for (auto _ : state) {
        DoNotOptimize(list);
        list.reverse();
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(reverseListSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void sortListSmall(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Small> list(size);
    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        list.push_back(small[i]);
    }


    for (auto _ : state) {
        DoNotOptimize(list);
        list.sort();
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(sortListSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void uniqueListSmall(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Small> list(size);
    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        list.push_back(small[i]);
    }


    for (auto _ : state) {

        DoNotOptimize(list);
        list.unique();
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(uniqueListSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void sizeMultiSetSmall(State &state) {


    auto N = state.range(0);

    std::multiset<Small> multiset;
    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        multiset.insert(small[i]);
    }


    for (auto _ : state) {
        auto t=multiset.size();
        DoNotOptimize(t);

    }

    state.SetComplexityN(N);
}

BENCHMARK(sizeMultiSetSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void max_sizeMultiSetSmall(State &state) {


    auto N = state.range(0);


    std::multiset<Small> multiset;
    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        multiset.insert(small[i]);
    }
    for (auto _ : state) {
        auto t = multiset.max_size();
        DoNotOptimize(t);

    }

    state.SetComplexityN(N);
}

BENCHMARK(max_sizeMultiSetSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void emptyMultiSetSmall(State &state) {


    auto N = state.range(0);



    std::multiset<Small> multiset;
    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        multiset.insert(small[i]);
    }
    for (auto _ : state) {

        auto t=multiset.empty();
        DoNotOptimize(t);

    }

    state.SetComplexityN(N);
}

BENCHMARK(emptyMultiSetSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void clearMultiSetSmall(State &state) {


    auto N = state.range(0);



    std::multiset<Small> multiset;
    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        multiset.insert(small[i]);
    }
    for (auto _ : state) {

        DoNotOptimize(multiset);

        multiset.clear();
        ClobberMemory();

    }

    state.SetComplexityN(N);
}

BENCHMARK(clearMultiSetSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void insertMultiSetSmall(State &state) {


    auto N = state.range(0);


    std::multiset<Small> multiset;
    Small small;
    small.data[0]='1';

    for (auto _ : state) {

        auto t=multiset.insert(small);
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(insertMultiSetSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void eraseMultiSetSmall(State &state) {


    auto N = state.range(0);


    std::multiset<Small> multiset;
    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        multiset.insert(small[i]);
    }
    auto el = small[0];
    for (auto _ : state) {
        auto t=multiset.erase(el);
        DoNotOptimize(t);

    }

    state.SetComplexityN(N);
}

BENCHMARK(eraseMultiSetSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void swapMultiSetSmall(State &state) {


    auto N = state.range(0);


    std::multiset<Small> multiset;

    std::multiset<Small> multiset2;
    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        multiset.insert(small[i]);
        multiset2.insert(small[i]);
    }

    for (auto _ : state) {
        DoNotOptimize(multiset);
        multiset.swap(multiset2);
        ClobberMemory();

    }

    state.SetComplexityN(N);
}

BENCHMARK(swapMultiSetSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void countMultiSetSmall(State &state) {


    auto N = state.range(0);


    std::multiset<Small> multiset;
    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        multiset.insert(small[i]);
    }
    auto el =small[0];

    for (auto _ : state) {

        auto t=  multiset.count(el);
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(countMultiSetSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void findMultiSetSmall(State &state) {


    auto N = state.range(0);


    std::multiset<Small> multiset;
    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        multiset.insert(small[i]);
    }


    for (auto _ : state) {
        auto t=multiset.find(Small{2});
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(findMultiSetSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void equal_rangeMultiSetSmall(State &state) {


    auto N = state.range(0);


    std::multiset<Small> multiset;

    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        multiset.insert(small[i]);
    }
    auto el =small[0];

    for (auto _ : state) {
        auto t=multiset.equal_range(el);
        DoNotOptimize(t);

    }

    state.SetComplexityN(N);
}

BENCHMARK(equal_rangeMultiSetSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void upper_boundMultiSetSmall(State &state) {


    auto N = state.range(0);

    std::multiset<Small> multiset;
    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        multiset.insert(small[i]);
    }
    auto el =small[0];


    for (auto _ : state) {
        auto t=multiset.upper_bound(el);
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(upper_boundMultiSetSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void sizeUnordered_MultiSetSmall(State &state) {


    auto N = state.range(0);

    std::unordered_multiset<Small> unordered_multiset;
    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        unordered_multiset.insert(small[i]);
    }



    for (auto _ : state) {
        auto t = unordered_multiset.size();
        DoNotOptimize(t);

    }

    state.SetComplexityN(N);
}

BENCHMARK(sizeUnordered_MultiSetSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void max_sizeUnordered_MultiSetSmall(State &state) {


    auto N = state.range(0);


    std::unordered_multiset<Small> unordered_multiset;
    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        unordered_multiset.insert(small[i]);
    }

    for (auto _ : state) {
        auto t = unordered_multiset.max_size();
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(max_sizeUnordered_MultiSetSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void emptyUnordered_MultiSetSmall(State &state) {


    auto N = state.range(0);


    std::unordered_multiset<Small> unordered_multiset;
    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        unordered_multiset.insert(small[i]);
    }
    for (auto _ : state) {
        auto t = unordered_multiset.empty();
        DoNotOptimize(t);

    }

    state.SetComplexityN(N);
}

BENCHMARK(emptyUnordered_MultiSetSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void clearUnordered_MultiSetSmall(State &state) {


    auto N = state.range(0);


    std::unordered_multiset<Small> unordered_multiset;
    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        unordered_multiset.insert(small[i]);
    }
    for (auto _ : state) {
        DoNotOptimize(unordered_multiset);

        unordered_multiset.clear();
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(clearUnordered_MultiSetSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void insertUnordered_MultiSetSmall(State &state) {


    auto N = state.range(0);


    std::unordered_multiset<Small> unordered_multiset;
    Small small;
    small.data[0]='1';
    for (auto _ : state) {
        DoNotOptimize(unordered_multiset);

        auto t=unordered_multiset.insert(small);
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(insertUnordered_MultiSetSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void eraseUnordered_MultiSetSmall(State &state) {


    auto N = state.range(0);



    std::unordered_multiset<Small> unordered_multiset;
    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        unordered_multiset.insert(small[i]);
    }
    auto el = small[0];
    for (auto _ : state) {
        auto t=unordered_multiset.erase(el);
        DoNotOptimize(t);

    }

    state.SetComplexityN(N);
}

BENCHMARK(eraseUnordered_MultiSetSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void swapUnordered_MultiSetSmall(State &state) {


    auto N = state.range(0);


    std::unordered_multiset<Small> unordered_multiset;
    std::unordered_multiset<Small> unordered_multiset2;
    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        unordered_multiset.insert(small[i]);
        unordered_multiset2.insert(small[i]);
    }

    for (auto _ : state) {
        DoNotOptimize(unordered_multiset);

        unordered_multiset.swap(unordered_multiset2);
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(swapUnordered_MultiSetSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void countUnordered_MultiSetSmall(State &state) {


    auto N = state.range(0);


    std::unordered_multiset<Small> unordered_multiset;
    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        unordered_multiset.insert(small[i]);
    }
    auto el=small[0];

    for (auto _ : state) {

        auto t=unordered_multiset.count(el);
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(countUnordered_MultiSetSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void findUnordered_MultiSetSmall(State &state) {


    auto N = state.range(0);


    std::unordered_multiset<Small> unordered_multiset = {{Small{2}}};
    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        unordered_multiset.insert(small[i]);
    }
    auto el=small[0];


    for (auto _ : state) {


        auto t=unordered_multiset.find(el);
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(findUnordered_MultiSetSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void equal_rangeUnordered_MultiSetSmall(State &state) {


    auto N = state.range(0);


    std::unordered_multiset<Small> unordered_multiset;
    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        unordered_multiset.insert(small[i]);
    }
    auto el=small[0];


    for (auto _ : state) {

        auto t=unordered_multiset.equal_range(el);
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(equal_rangeUnordered_MultiSetSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void reserveUnordered_MultiSetSmall(State &state) {


    auto N = state.range(0);


    std::unordered_multiset<Small> unordered_multiset;
    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        unordered_multiset.insert(small[i]);
    }

    for (auto _ : state) {
        DoNotOptimize(unordered_multiset);

        unordered_multiset.reserve(5);
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(reserveUnordered_MultiSetSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();

void rehashUnordered_MultiSetSmall(State &state) {


    auto N = state.range(0);

    std::unordered_multiset<Small> unordered_multiset;
    Small small[N];
    for(unsigned int i =0;i<N;i++)
    {
        small[i].randomize();
        unordered_multiset.insert(small[i]);
    }
    for (auto _ : state) {
        DoNotOptimize(unordered_multiset);
        unordered_multiset.rehash(7);
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(rehashUnordered_MultiSetSmall)->RangeMultiplier(2)->Range(1, 512)->Complexity();
