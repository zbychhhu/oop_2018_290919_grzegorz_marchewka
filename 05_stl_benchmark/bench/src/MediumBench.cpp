#include "Medium.h"
#include "BenchIncludes.h"
#include <list>
#include <set>
#include <unordered_set>

// TODO: Add benchmarks for operator<. operator==, and hash

void lessOperatorMedium(State &state)
{

    Medium medium{};
    Medium medium2{};
    medium.randomize();
    medium2.randomize();
    bool condition;
    for(auto _ : state)
    {
        condition=medium<medium2;
        DoNotOptimize(condition);
    }

}
BENCHMARK(lessOperatorMedium)->RangeMultiplier(2)->Range(1,1024);

void equalityMedium(State &state)
{

    Medium medium{};
    Medium medium2{};
    medium.randomize();
    medium2.randomize();
    bool condition;
    for(auto _ : state)
    {
        condition=medium==medium2;
        DoNotOptimize(condition);
    }

}
BENCHMARK(equalityMedium)->RangeMultiplier(2)->Range(1,1024);

void hashMedium(State &state)
{

    Medium medium{};
    std::hash<Medium> hash;
    medium.randomize();
    for(auto _ : state)
    {
        hash(medium);
        DoNotOptimize(medium);

    }
}
BENCHMARK(hashMedium)->RangeMultiplier(2)->Range(1,1024);

void sizeListMedium(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Medium> list(size);
    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        list.push_back(medium[i]);
    }

    for (auto _ : state) {
        DoNotOptimize(list.size());

    }

    state.SetComplexityN(N);
}

BENCHMARK(sizeListMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void frontListMedium(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Medium> list(size);
    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        list.push_back(medium[i]);
    }

    for (auto _ : state) {
        auto t=list.front();
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(frontListMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void backListMedium(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Medium> list(size);
    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        list.push_back(medium[i]);
    }

    for (auto _ : state) {
        auto t=list.back();
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(backListMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void emptyListMedium(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Medium> list(size);
    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        list.push_back(medium[i]);
    }

    for (auto _ : state) {
        auto t=list.empty();
        DoNotOptimize(t);

    }

    state.SetComplexityN(N);
}

BENCHMARK(emptyListMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void max_sizeListMedium(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Medium> list(size);
    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        list.push_back(medium[i]);
    }

    for (auto _ : state) {
        auto t=list.max_size();
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(max_sizeListMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void eraseListMedium(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Medium> list(size);
    auto it = list.begin();
    for (auto _ : state) {
        list.push_back(Medium{2});
        auto t=list.erase(it);
        DoNotOptimize(t);


    }

    state.SetComplexityN(N);
}

BENCHMARK(eraseListMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void insertListMedium(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Medium> list(size);
    Medium medium[N];
    medium[0].data[0]='1';
    auto el =medium[0];
    auto iter = list.begin();

    for (auto _ : state) {
        DoNotOptimize(list);

        auto t=list.insert(iter,el);
    }

    state.SetComplexityN(N);
}

BENCHMARK(insertListMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void clearListMedium(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Medium> list(size);
    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        list.push_back(medium[i]);
    }

    for (auto _ : state) {
        DoNotOptimize(list);
        list.clear();
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(clearListMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void push_frontAndpop_frontListMedium(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;

    Medium medium{};
    *medium.data = '2';
    std::list<Medium> list(size);

    for (auto _ : state) {
        DoNotOptimize(list);
        list.push_front(medium);
        list.pop_front();
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(push_frontAndpop_frontListMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void push_backAndpop_backListMedium(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;

    Medium medium{};
    *medium.data = '2';
    std::list<Medium> list(size);

    for (auto _ : state) {
        DoNotOptimize(list);
        list.push_back(medium);
        list.pop_back();
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(push_backAndpop_backListMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void resizeListMedium(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Medium> list(size);
    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        list.push_back(medium[i]);
    }

    for (auto _ : state) {
        DoNotOptimize(list);
        list.resize(3);
        ClobberMemory();
    }


    state.SetComplexityN(N);
}

BENCHMARK(resizeListMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void swapListMedium(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Medium> list(size);
    std::list<Medium> list2(size);
    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        list.push_back(medium[i]);
        list2.push_front(medium[i]);
    }
    for (auto _ : state) {

        list.swap(list2);
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(swapListMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();


void mergeListMedium(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Medium> list(size);
    std::list<Medium> list2(size);
    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        list.push_back(medium[i]);
        list2.push_front(medium[i]);
    }

    for (auto _ : state) {
        DoNotOptimize(list);
        DoNotOptimize(list2);
        list.merge(list2);
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(mergeListMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void spliceListMedium(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Medium> list(size);
    std::list<Medium> list2(size);
    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        list.push_back(medium[i]);
        list2.push_front(medium[i]);
    }
    auto it = list.begin();
    for (auto _ : state) {

        DoNotOptimize(list);
        DoNotOptimize(list2);
        list.splice(it, list2);
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(spliceListMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void removeListMedium(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Medium> list(size);
    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        list.push_back(medium[i]);
    }
    auto el = medium[0];


    for (auto _ : state) {

        DoNotOptimize(list);
        list.remove(el);
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(removeListMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void remove_ifListMedium(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Medium> list(size);
    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        list.push_back(medium[i]);
    }


    for (auto _ : state) {
        DoNotOptimize(list);
        list.remove_if([](Medium n) { return n < Medium{'1'}; });
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(remove_ifListMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void reverseListMedium(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Medium> list(size);
    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        list.push_back(medium[i]);
    }


    for (auto _ : state) {
        DoNotOptimize(list);
        list.reverse();
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(reverseListMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void sortListMedium(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Medium> list(size);
    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        list.push_back(medium[i]);
    }


    for (auto _ : state) {
        DoNotOptimize(list);
        list.sort();
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(sortListMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void uniqueListMedium(State &state) {


    auto N = state.range(0);
    auto size = (std::size_t) N;


    std::list<Medium> list(size);
    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        list.push_back(medium[i]);
    }


    for (auto _ : state) {

        DoNotOptimize(list);
        list.unique();
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(uniqueListMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void sizeMultiSetMedium(State &state) {


    auto N = state.range(0);

    std::multiset<Medium> multiset;
    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        multiset.insert(medium[i]);
    }


    for (auto _ : state) {
        auto t=multiset.size();
        DoNotOptimize(t);

    }

    state.SetComplexityN(N);
}

BENCHMARK(sizeMultiSetMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void max_sizeMultiSetMedium(State &state) {


    auto N = state.range(0);


    std::multiset<Medium> multiset;
    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        multiset.insert(medium[i]);
    }
    for (auto _ : state) {
        auto t = multiset.max_size();
        DoNotOptimize(t);

    }

    state.SetComplexityN(N);
}

BENCHMARK(max_sizeMultiSetMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void emptyMultiSetMedium(State &state) {


    auto N = state.range(0);



    std::multiset<Medium> multiset;
    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        multiset.insert(medium[i]);
    }
    for (auto _ : state) {

        auto t=multiset.empty();
        DoNotOptimize(t);

    }

    state.SetComplexityN(N);
}

BENCHMARK(emptyMultiSetMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void clearMultiSetMedium(State &state) {


    auto N = state.range(0);



    std::multiset<Medium> multiset;
    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        multiset.insert(medium[i]);
    }
    for (auto _ : state) {

        DoNotOptimize(multiset);

        multiset.clear();
        ClobberMemory();

    }

    state.SetComplexityN(N);
}

BENCHMARK(clearMultiSetMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void insertMultiSetMedium(State &state) {


    auto N = state.range(0);


    std::multiset<Medium> multiset;
    Medium medium;
    medium.data[0]='1';

    for (auto _ : state) {

        auto t=multiset.insert(medium);
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(insertMultiSetMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void eraseMultiSetMedium(State &state) {


    auto N = state.range(0);


    std::multiset<Medium> multiset;
    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        multiset.insert(medium[i]);
    }
    auto el = medium[0];
    for (auto _ : state) {
        auto t=multiset.erase(el);
        DoNotOptimize(t);

    }

    state.SetComplexityN(N);
}

BENCHMARK(eraseMultiSetMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void swapMultiSetMedium(State &state) {


    auto N = state.range(0);


    std::multiset<Medium> multiset;

    std::multiset<Medium> multiset2;
    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        multiset.insert(medium[i]);
        multiset2.insert(medium[i]);
    }

    for (auto _ : state) {
        DoNotOptimize(multiset);
        multiset.swap(multiset2);
        ClobberMemory();

    }

    state.SetComplexityN(N);
}

BENCHMARK(swapMultiSetMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void countMultiSetMedium(State &state) {


    auto N = state.range(0);


    std::multiset<Medium> multiset;
    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        multiset.insert(medium[i]);
    }
    auto el =medium[0];

    for (auto _ : state) {

        auto t=  multiset.count(el);
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(countMultiSetMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void findMultiSetMedium(State &state) {


    auto N = state.range(0);


    std::multiset<Medium> multiset;
    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        multiset.insert(medium[i]);
    }


    for (auto _ : state) {
        auto t=multiset.find(Medium{2});
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(findMultiSetMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void equal_rangeMultiSetMedium(State &state) {


    auto N = state.range(0);


    std::multiset<Medium> multiset;

    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        multiset.insert(medium[i]);
    }
    auto el =medium[0];

    for (auto _ : state) {
        auto t=multiset.equal_range(el);
        DoNotOptimize(t);

    }

    state.SetComplexityN(N);
}

BENCHMARK(equal_rangeMultiSetMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void upper_boundMultiSetMedium(State &state) {


    auto N = state.range(0);

    std::multiset<Medium> multiset;
    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        multiset.insert(medium[i]);
    }
    auto el =medium[0];


    for (auto _ : state) {
        auto t=multiset.upper_bound(el);
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(upper_boundMultiSetMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void sizeUnordered_MultiSetMedium(State &state) {


    auto N = state.range(0);

    std::unordered_multiset<Medium> unordered_multiset;
    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        unordered_multiset.insert(medium[i]);
    }



    for (auto _ : state) {
        auto t = unordered_multiset.size();
        DoNotOptimize(t);

    }

    state.SetComplexityN(N);
}

BENCHMARK(sizeUnordered_MultiSetMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void max_sizeUnordered_MultiSetMedium(State &state) {


    auto N = state.range(0);


    std::unordered_multiset<Medium> unordered_multiset;
    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        unordered_multiset.insert(medium[i]);
    }

    for (auto _ : state) {
        auto t = unordered_multiset.max_size();
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(max_sizeUnordered_MultiSetMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void emptyUnordered_MultiSetMedium(State &state) {


    auto N = state.range(0);


    std::unordered_multiset<Medium> unordered_multiset;
    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        unordered_multiset.insert(medium[i]);
    }
    for (auto _ : state) {
        auto t = unordered_multiset.empty();
        DoNotOptimize(t);

    }

    state.SetComplexityN(N);
}

BENCHMARK(emptyUnordered_MultiSetMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void clearUnordered_MultiSetMedium(State &state) {


    auto N = state.range(0);


    std::unordered_multiset<Medium> unordered_multiset;
    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        unordered_multiset.insert(medium[i]);
    }
    for (auto _ : state) {
        DoNotOptimize(unordered_multiset);

        unordered_multiset.clear();
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(clearUnordered_MultiSetMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void insertUnordered_MultiSetMedium(State &state) {


    auto N = state.range(0);


    std::unordered_multiset<Medium> unordered_multiset;
    Medium medium;
    medium.data[0]='1';
    for (auto _ : state) {
        DoNotOptimize(unordered_multiset);

        auto t=unordered_multiset.insert(medium);
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(insertUnordered_MultiSetMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void eraseUnordered_MultiSetMedium(State &state) {


    auto N = state.range(0);



    std::unordered_multiset<Medium> unordered_multiset;
    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        unordered_multiset.insert(medium[i]);
    }
    auto el = medium[0];
    for (auto _ : state) {
        auto t=unordered_multiset.erase(el);
        DoNotOptimize(t);

    }

    state.SetComplexityN(N);
}

BENCHMARK(eraseUnordered_MultiSetMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void swapUnordered_MultiSetMedium(State &state) {


    auto N = state.range(0);


    std::unordered_multiset<Medium> unordered_multiset;
    std::unordered_multiset<Medium> unordered_multiset2;
    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        unordered_multiset.insert(medium[i]);
        unordered_multiset2.insert(medium[i]);
    }

    for (auto _ : state) {
        DoNotOptimize(unordered_multiset);

        unordered_multiset.swap(unordered_multiset2);
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(swapUnordered_MultiSetMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void countUnordered_MultiSetMedium(State &state) {


    auto N = state.range(0);


    std::unordered_multiset<Medium> unordered_multiset;
    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        unordered_multiset.insert(medium[i]);
    }
    auto el=medium[0];

    for (auto _ : state) {

        auto t=unordered_multiset.count(el);
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(countUnordered_MultiSetMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void findUnordered_MultiSetMedium(State &state) {


    auto N = state.range(0);


    std::unordered_multiset<Medium> unordered_multiset = {{Medium{2}}};
    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        unordered_multiset.insert(medium[i]);
    }
    auto el=medium[0];


    for (auto _ : state) {


        auto t=unordered_multiset.find(el);
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(findUnordered_MultiSetMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void equal_rangeUnordered_MultiSetMedium(State &state) {


    auto N = state.range(0);


    std::unordered_multiset<Medium> unordered_multiset;
    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        unordered_multiset.insert(medium[i]);
    }
    auto el=medium[0];


    for (auto _ : state) {

        auto t=unordered_multiset.equal_range(el);
        DoNotOptimize(t);
    }

    state.SetComplexityN(N);
}

BENCHMARK(equal_rangeUnordered_MultiSetMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void reserveUnordered_MultiSetMedium(State &state) {


    auto N = state.range(0);


    std::unordered_multiset<Medium> unordered_multiset;
    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        unordered_multiset.insert(medium[i]);
    }

    for (auto _ : state) {
        DoNotOptimize(unordered_multiset);

        unordered_multiset.reserve(5);
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(reserveUnordered_MultiSetMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();

void rehashUnordered_MultiSetMedium(State &state) {


    auto N = state.range(0);

    std::unordered_multiset<Medium> unordered_multiset;
    Medium medium[N];
    for(unsigned int i =0;i<N;i++)
    {
        medium[i].randomize();
        unordered_multiset.insert(medium[i]);
    }
    for (auto _ : state) {
        DoNotOptimize(unordered_multiset);
        unordered_multiset.rehash(7);
        ClobberMemory();
    }

    state.SetComplexityN(N);
}

BENCHMARK(rehashUnordered_MultiSetMedium)->RangeMultiplier(2)->Range(1, 128)->Complexity();
