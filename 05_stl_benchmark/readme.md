# REPORT





##1. Missing operators and hash functions
        1. My less-than operator for N-dimensional data adds all elements in two tables and compare result of sum.
        2. In equality operator we compare elements of the same index,when all of elements are equal then tables are equal.
        3. A hash function is any function that can be used to map data of arbitrary size to data of a fixed size.
        The values returned by a hash function are called hash values. Hash functions accelerate table or database
        lookup by detecting duplicated records in a large file.
        


##2. Unit test for operators and hash functions
    
    1. We shouldn't initialize objects with random data,because we won't get to know if 
    out test works properly.
    2.We should initialize objects manually to check correctness   
    
##3. Benchmarks for operators and hash functions
    
    1. I initialize two objects and compare they in for loop.
    2. If containers are bigger then benchmarks have longer duration.
    3. I receive smallest time for Small objects ,and biggest time for  Large objects.
    4. We should initialize objects (Small/Medium/Large) with random data that receive
    expected equality.
    
##4. Benchmarks for assigned containers/methods using [Small](include/Small.h) type and _Debug_ build
    
###1.List
  ![Image](output/SizeListSmallDebug.png "SizeListSmall")
    ![Image](output/UniqueLSD.png " Unique")
    ![Image](output/SwapListSmallDebug.png " SwapListSmall")
    ![Image](output/SpliceLSD.png " SpliceListSmall")
    ![Image](output/SortLSD.png " SortListSmall")
    ![Image](output/ReverseLSD.png " ReverseListSmall")
    ![Image](output/ResizeListSmallDebug.png " ResizeListSmall")
    ![Image](output/RemoveLSD.png " RemoveListSmall")
    ![Image](output/Remove_ifLSD.png " Remove_ifListSmall")
    ![Image](output/PUFrontListSmallDebug.png " SizeListSmall")
    ![Image](output/PUBackListSmallDebug.png " SizeListSmall")
    ![Image](output/MergeSmallListDebug.png " SizeListSmall")
    ![Image](output/MaxSizeListSmallDebug.png " SizeListSmall")
    ![Image](output/InsertListSmallDebug.png " SizeListSmall")
    ![Image](output/FrontListSmallDebug.png " SizeListSmall")
    ![Image](output/EraseListSmall.png " SizeListSmall")
    ![Image](output/EmptyListSmallDeug.png " SizeListSmall")
    ![Image](output/ClearListSmallDebug.png " SizeListSmall")
    ![Image](output/BackListSmallDebug.png " SizeListSmall")
###Multiset
   ![Image](output/clearM.png)
   ![Image](output/countM.png)
   ![Image](output/emptyM.png)
   ![Image](output/equalM.png)
   ![Image](output/eraseM.png)
   ![Image](output/findM.png)
   ![Image](output/insertM.png)
   ![Image](output/maxsizeM.png)
   ![Image](output/sizeM.png)
   ![Image](output/swapM.png)
   ![Image](output/upperM.png)
###Unordered_Multiset
   ![Image](output/clearUM.png)
      ![Image](output/countUM.png)
      ![Image](output/emptyUM.png)
      ![Image](output/equalUM.png)
      ![Image](output/eraseUM.png)
      ![Image](output/findUM.png)
      ![Image](output/insertUM.png)
      ![Image](output/maxsizeUM.png)
      ![Image](output/sizeUM.png)
      ![Image](output/swapUM.png)
      ![Image](output/rehashUM.png)
       ![Image](output/reserveUM.png)
   
       
##5. Release
    
    1.Plot of benchmarks in release mode are very similiar ,but we have
     some differents.
    
    
##6.  [Medium](include/Medium.h) and [Large](include/Large.h) types

    1. The bigger size is the bigger duration we get.
    2.In Release mode we can use ClobberMemory() and DoNotOptimize() , Release mode 
    during shorter than benchmark in debug mode. 
    
    
